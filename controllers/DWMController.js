'use strict';
var mongoose = require('mongoose');
require('../models/crm.js');
require('../models/five9.js');
var Crm = mongoose.model('CRM');
var Occ = mongoose.model('Occupancy');
var moment = require('moment');
var gf = require('./generalFunction.js');


exports.getOneDayOfMonth = function(req, res) {
  var dateArray = new Object();
  dateArray.date = req.body.day + "/" + req.body.month + "/" + req.body.year;
  Crm.find({"year": req.body.year, "month": req.body.month, "day": req.body.day}, function(err, data) {
    dateArray.calls = data.length;
    console.log(data.length);
    if(data.length == 0) {
      res.send("404");
    } else {
      dateArray.week = data[0].week;
      Crm.distinct("phone", {year: req.body.year, month: req.body.month, day: req.body.day}, function(err, data){
        dateArray.leads = data.length;
        Occ.find({"year": req.body.year, "month": req.body.month, "day": req.body.day}, function(err, data) {
          // dateArray.onCall = moment.utc((data[0].oncall + data[0].acw)*3600000).format("HH:mm:ss");
          dateArray.onCall = Math.round((data[0].oncall + data[0].acw) * 100) /100;
          console.log("on call: " + dateArray.onCall);
          dateArray.aht = moment.utc(((data[0].oncall + data[0].acw) / dateArray.calls) * 3600000).format("HH:mm:ss");
          dateArray.occ = Math.round(((dateArray.onCall / (data[0].oncall + data[0].acw + data[0].ringing + data[0].ready)) * 100) * 100) /100;
          Crm.find({"year": req.body.year, "month": req.body.month, "day": req.body.day, stage:"01 No Answer"}, function(err, data) {
            dateArray.noAnswer = data.length;
            Crm.find({"year": req.body.year, "month": req.body.month, "day": req.body.day, stage: "02 Greeting"}, function(err, data) {
              dateArray.greeting = data.length;
              Crm.find({"year": req.body.year, "month": req.body.month, "day": req.body.day, stage:"03 Gate Keeper"}, function(err, data) {
                dateArray.gatekeeper = data.length;
                Crm.find({"year": req.body.year, "month": req.body.month, "day": req.body.day, stage:"04 UPS Made"}, function(err, data) {
                  dateArray.ups = data.length;
                  Crm.find({"year": req.body.year, "month": req.body.month, "day": req.body.day, stage:"05 Pitch Made"}, function(err, data) {
                    dateArray.pitch = data.length;
                    Crm.find({"year": req.body.year, "month": req.body.month, "day": req.body.day, stage:"06 Provided Rate"}, function (err, data) {
                      dateArray.provided = data.length;
                      Crm.find({"year": req.body.year, "month": req.body.month, "day": req.body.day, "disposition": ""}, function(err, data) {
                        dateArray.missing = data.length;
                        dateArray.salespitch = dateArray.ups + dateArray.pitch + dateArray.provided;
                        dateArray.answered = dateArray.greeting + dateArray.gatekeeper + dateArray.salespitch;
                        dateArray.percentage = Math.round(((dateArray.answered / dateArray.calls) * 100) * 100) /100;
                        Crm.find({"year": req.body.year, "month": req.body.month, "day": req.body.day, "status": "Opportunity Lost"}, function(err, data) {
                          dateArray.lost = data.length;
                          Crm.find({"year": req.body.year, "month": req.body.month, "day": req.body.day, "status": "Opportunity Working"}, function(err, data) {
                            dateArray.work = data.length;
                            res.send(dateArray);
                          });
                        });
                      });
                    });
                  });
                });
              });
            });
          });
        });
      });
    }
  });
}

exports.getOneWeekOfMonth = function(req, res) {
  var dateArray = new Object();
  Crm.find({"year": req.body.year, "week": req.body.week}, function(err, data) {
    dateArray.calls = data.length;
    dateArray.week = req.body.week;
    Crm.distinct("phone", {year: req.body.year, week: req.body.week}, function(err, data){
      dateArray.leads = data.length;
      Occ.find({"year": req.body.year, "week": req.body.week}, function(err, data) {
        // dateArray.onCall = moment.utc((data[0].oncall + data[0].acw)*3600000).format("HH:mm:ss");
        var oc = 0;
        var occ = 0;
        for (var j = 0; j < data.length; j++) {
          oc += data[j].oncall + data[j].acw;
          occ += data[j].oncall + data[j].acw + data[j].ringing + data[j].ready;
        }
        dateArray.onCall = Math.round(oc * 100) / 100;
        dateArray.aht = moment.utc(((oc) / dateArray.calls) * 3600000).format("HH:mm:ss");
        dateArray.occ = Math.round(((dateArray.onCall / occ) * 100) * 100) /100;
        Crm.find({"year": req.body.year, "week": req.body.week, stage:"01 No Answer"}, function(err, data) {
          dateArray.noAnswer = data.length;
          Crm.find({"year": req.body.year, "week": req.body.week, stage: "02 Greeting"}, function(err, data) {
            dateArray.greeting = data.length;
            Crm.find({"year": req.body.year, "week": req.body.week, stage:"03 Gate Keeper"}, function(err, data) {
              dateArray.gatekeeper = data.length;
              Crm.find({"year": req.body.year, "week": req.body.week, stage:"04 UPS Made"}, function(err, data) {
                dateArray.ups = data.length;
                Crm.find({"year": req.body.year, "week": req.body.week, stage:"05 Pitch Made"}, function(err, data) {
                  dateArray.pitch = data.length;
                  Crm.find({"year": req.body.year, "week": req.body.week, stage:"06 Provided Rate"}, function (err, data) {
                    dateArray.provided = data.length;
                    Crm.find({"year": req.body.year, "week": req.body.week, "disposition": ""}, function(err, data) {
                      dateArray.missing = data.length;
                      dateArray.salespitch = dateArray.ups + dateArray.pitch + dateArray.provided;
                      dateArray.answered = dateArray.greeting + dateArray.gatekeeper + dateArray.salespitch
                      dateArray.percentage = Math.round(((dateArray.answered / dateArray.calls) * 100) * 100) /100;
                      Crm.find({"year": req.body.year, "week": req.body.week, "status": "Opportunity Lost"}, function(err, data) {
                        dateArray.lost = data.length;
                        Crm.find({"year": req.body.year, "week": req.body.week, "status": "Opportunity Working"}, function(err, data) {
                          dateArray.work = data.length;
                          res.send(dateArray);
                        });
                      });
                    });
                  });
                });
              });
            });
          });
        });
      });
    });
  });
}

exports.getOneMonth = function(req, res) {
  var dateArray = new Object();
  Crm.find({"year": req.body.year, "month": req.body.month}, function(err, data) {
    dateArray.calls = data.length;
    dateArray.month = req.body.month;
    Crm.distinct("phone", {year: req.body.year, month: req.body.month}, function(err, data){
      dateArray.leads = data.length;
      Occ.find({"year": req.body.year, "month": req.body.month}, function(err, data) {
        // dateArray.onCall = moment.utc((data[0].oncall + data[0].acw)*3600000).format("HH:mm:ss");
        // dateArray.onCall = moment.utc((data[0].oncall + data[0].acw)*3600000).format("HH:mm:ss");
        var oc = 0;
        var occ = 0;
        for (var j = 0; j < data.length; j++) {
          oc += data[j].oncall + data[j].acw;
          occ += data[j].oncall + data[j].acw + data[j].ringing + data[j].ready;
        }
        dateArray.onCall = Math.round(oc * 100) / 100;
        dateArray.aht = moment.utc(((oc) / dateArray.calls) * 3600000).format("HH:mm:ss");
        dateArray.occ = Math.round(((dateArray.onCall / occ) * 100) * 100) /100;
        Crm.find({"year": req.body.year, "month": req.body.month, stage:"01 No Answer"}, function(err, data) {
          dateArray.noAnswer = data.length;
          Crm.find({"year": req.body.year, "month": req.body.month, stage: "02 Greeting"}, function(err, data) {
            dateArray.greeting = data.length;
            Crm.find({"year": req.body.year, "month": req.body.month, stage:"03 Gate Keeper"}, function(err, data) {
              dateArray.gatekeeper = data.length;
              Crm.find({"year": req.body.year, "month": req.body.month, stage:"04 UPS Made"}, function(err, data) {
                dateArray.ups = data.length;
                Crm.find({"year": req.body.year, "month": req.body.month, stage:"05 Pitch Made"}, function(err, data) {
                  dateArray.pitch = data.length;
                  Crm.find({"year": req.body.year, "month": req.body.month, stage:"06 Provided Rate"}, function (err, data) {
                    dateArray.provided = data.length;
                    Crm.find({"year": req.body.year, "month": req.body.month, "disposition": ""}, function(err, data) {
                      dateArray.missing = data.length;
                      dateArray.salespitch = dateArray.ups + dateArray.pitch + dateArray.provided;
                      dateArray.answered = dateArray.greeting + dateArray.gatekeeper + dateArray.salespitch;
                      dateArray.percentage = Math.round(((dateArray.answered / dateArray.calls) * 100) * 100) /100;
                      Crm.find({"year": req.body.year, "month": req.body.month, "status": "Opportunity Lost"}, function(err, data) {
                        dateArray.lost = data.length;
                        Crm.find({"year": req.body.year, "month": req.body.month, "status": "Opportunity Working"}, function(err, data) {
                          dateArray.work = data.length;
                          res.send(dateArray);
                        });
                      });
                    });
                  });
                });
              });
            });
          });
        });
      });
    });
  });
}
