'use strict';
var mongoose = require('mongoose');
require('../models/crm.js');
require('../models/five9.js');
var Crm = mongoose.model('CRM');
var Occ = mongoose.model('Occupancy');
var moment = require('moment');
var gf = require('./generalFunction.js');

exports.getBatchData = function(req, res) {
  var batchArray = new Object();
  Crm.distinct("phone", {batch: req.body.batch}, function(err, data) {
    batchArray.leads = data.length;
    Crm.distinct("phone",{"status": "Opportunity Lost", "batch": req.body.batch}, function(err, data) {
      batchArray.lost = data.length;
      Crm.distinct("phone",{"status": "Opportunity Working", "batch": req.body.batch}, function(err, data) {
          batchArray.work = data.length;
          Crm.distinct("phone", {"status": "Qualified", "batch": req.body.batch}, function(err, data) {
            batchArray.qualified = data.length;
            Crm.find({"batch": req.body.batch}, function(err, data) {
              batchArray.calls = data.length;
              Crm.find({"stage": "01 No Answer", "batch": req.body.batch}, function(err, data) {
                batchArray.noAnswer = data.length;
                Crm.find({"stage": "02 Greeting", "batch": req.body.batch}, function(err, data) {
                  batchArray.greeting = data.length;
                  Crm.find({"stage": "03 Gate Keeper", "batch": req.body.batch}, function(err, data) {
                    batchArray.gatekeeper = data.length;
                    Crm.find({"stage": "04 UPS Made", "batch": req.body.batch}, function(err, data) {
                      batchArray.ups = data.length;
                      Crm.find({"stage": "05 Pitch Made", "batch": req.body.batch}, function(err, data) {
                        batchArray.pitch = data.length;
                        Crm.find({"stage": "06 Provided Rate", "batch": req.body.batch}, function(err, data) {
                          batchArray.provided = data.length;
                          batchArray.salespitch = batchArray.ups + batchArray.pitch + batchArray.provided;
                          batchArray.answered = batchArray.greeting + batchArray.gatekeeper + batchArray.salespitch;
                          res.send(batchArray);
                        });
                      });
                    });
                  });
                });
              });
            });
          });
      });
    });
  });
}

exports.getFullBatchData = function(req, res) {
  var batchArray = new Object();
  Crm.distinct("phone", function(err, data) {
    batchArray.leads = data.length;
    Crm.distinct("phone",{"status": "Opportunity Lost"}, function(err, data) {
      batchArray.lost = data.length;
      Crm.distinct("phone",{"status": "Opportunity Working"}, function(err, data) {
          batchArray.work = data.length;
          Crm.distinct("phone", {"status": "Qualified"}, function(err, data) {
            batchArray.qualified = data.length;
            Crm.find({}, function(err, data) {
              batchArray.calls = data.length;
              Crm.find({"stage": "01 No Answer"}, function(err, data) {
                batchArray.noAnswer = data.length;
                Crm.find({"stage": "02 Greeting"}, function(err, data) {
                  batchArray.greeting = data.length;
                  Crm.find({"stage": "03 Gate Keeper"}, function(err, data) {
                    batchArray.gatekeeper = data.length;
                    Crm.find({"stage": "04 UPS Made"}, function(err, data) {
                      batchArray.ups = data.length;
                      Crm.find({"stage": "05 Pitch Made"}, function(err, data) {
                        batchArray.pitch = data.length;
                        Crm.find({"stage": "06 Provided Rate"}, function(err, data) {
                          batchArray.provided = data.length;
                          batchArray.salespitch = batchArray.ups + batchArray.pitch + batchArray.provided;
                          batchArray.answered = batchArray.greeting + batchArray.gatekeeper + batchArray.salespitch;
                          res.send(batchArray);
                        });
                      });
                    });
                  });
                });
              });
            });
          });
      });
    });
  });
}
