'use strict';
var mongoose = require('mongoose');
require('../models/crm.js');
var Crm = mongoose.model('CRM');
var fs = require('fs');
var gf = require('./generalFunction.js');
var moment = require('moment');

exports.importData = function(req, res) {
  console.log(req.body.path);
  console.log("import");
  var c = 0;
  var linelist = fs.readFileSync(req.body.path).toString().split('\n');
  for (var i = 0;i < linelist.length - 1; i++) {
    var line = linelist[i].split(',');
    if(line.length != 0) {
      //console.log(line[0]);
      var crmData = new Crm({
        phone: (line[2] == "") ? 0 : line[2],
        disposition: line[3],
        stage: line[4],
        owner: line[5],
        status: line[6],
        batch: line[7],
        year: line[10],
        month: line[8],
        day: line[9],
        week: line[12]
      });

      crmData.save(function(err) {
        if(err)
          throw err;
        console.log(c);
        c++;
      });
    }
    // console.log(line);

  }
  res.jsonp("Added");
}

exports.getDailyData = function(req, res) {
  var leadsDialed = 0;
  var numberOfCalls = 0;
  var noAnswer = 0;
  console.log("dailydata");
  Crm.distinct("phone", {year: req.body.year, month: req.body.month, day:req.body.day}, function(err, data) {
    leadsDialed = data.length;
    Crm.find({"year": req.body.year, "month": req.body.month, "day": req.body.day}, function(err, data) {
      numberOfCalls = data.length;
      Crm.distinct("phone", {stage: '01 No Answer', year: req.body.year, month: req.body.month, day:req.body.day}, function(err, data) {
        noAnswer = data.length;
        res.jsonp({"leads": leadsDialed, "calls": numberOfCalls, "noanswer": noAnswer});
      });
    });
  });


  // console.log(leadsDialed);
  // console.log(numberOfCalls);
  // console.log(noAnswer);

}

exports.getWeeklyData = function(req, res) {
  var leadsDialed = 0;
  var numberOfCalls = 0;
  var noAnswer = 0;
  console.log(req.body.week);
  console.log("monthlyData");
  Crm.distinct("phone", {year: req.body.year, week: req.body.week}, function(err, data) {
    leadsDialed = data.length;
    Crm.find({"year": req.body.year, "week": req.body.week}, function(err, data) {
      numberOfCalls = data.length;
      Crm.distinct("phone", {stage: '01 No Answer', year: req.body.year, week: req.body.week}, function(err, data) {
        noAnswer = data.length;
        res.jsonp({"leads": leadsDialed, "calls": numberOfCalls, "noanswer": noAnswer});
      });
    });
  });

}


exports.getMonthlyData = function(req, res) {
  var leadsDialed = 0;
  var numberOfCalls = 0;
  var noAnswer = 0;
  console.log("monthlyData");

  Crm.distinct("phone", {year: req.body.year, month: req.body.month}, function(err, data) {
    leadsDialed = data.length;
    Crm.find({"year": req.body.year, "month": req.body.month}, function(err, data) {
      numberOfCalls = data.length;
      Crm.distinct("phone", {stage: '01 No Answer', year: req.body.year, month: req.body.month}, function(err, data) {
        noAnswer = data.length;
        res.jsonp({"leads": leadsDialed, "calls": numberOfCalls, "noanswer": noAnswer});
      });
    });

  });

}
