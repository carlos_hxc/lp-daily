
//Stages
var stages = ["01 No Answer", "02 Greeting, 03 Gate Keeper", "04 Pitch Made", "05 Pitch Made"];


//Function to convert number to Date JS
exports.excelDateToJS = function (dateNum) {
  return new Date((dateNum - 25569) * 86400 * 1000);
}

exports.notANumber = function(cell) {
  if(isNaN(cell)) {
    console.log("NAN");
    return 0;
  } else {
    return cell;
  }
}
