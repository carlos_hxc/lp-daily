'use strict';
var mongoose = require('mongoose');
require('../models/five9.js');
var Occ = mongoose.model('Occupancy');
var fs = require('fs');

exports.importData = function(req, res) {
  console.log("Import occ");
  var linelist = fs.readFileSync(req.body.path).toString().split("\n");
  for (var i = 0; i < linelist.length - 1; i++) {
    var line = linelist[i].split(',');
    console.log(line);
    var preFecha = line[0].split("/");
    var occData = new Occ({
      oncall: line[1] * 24,
      ringing: line[2] * 24,
      acw: line[3] * 24,
      ready: line[4] * 24,
      day: preFecha[0],
      month: preFecha[1],
      year: preFecha[2],
      week: line[5]
    });

    occData.save(function(err) {
      if(err)
        console.log(err);
    });
  }
  res.jsonp("Added");
}
