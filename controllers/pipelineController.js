'use strict';
'use strict';
var mongoose = require('mongoose');
require('../models/crm.js');
var Crm = mongoose.model('CRM');
var fs = require('fs');
var gf = require('./generalFunction.js');
var moment = require('moment');

exports.getAgents = function(req, res) {
  console.log("Agents");
  Crm.distinct("owner", {day: req.body.day, month: req.body.month, year: req.body.year}, function(err, data) {
    res.send(data);
  });
}

exports.getAgentsWeek = function(req, res) {
  console.log("Agents");
  Crm.distinct("owner", {week: req.body.week, year: req.body.year}, function(err, data) {
    res.send(data);
  });
}


exports.getAgentsMonth = function(req, res) {
  console.log("Agents");
  Crm.distinct("owner", {month: req.body.month, year: req.body.year}, function(err, data) {
    res.send(data);
  });
}


exports.getWeeks = function(req, res) {
  Crm.distinct("week", function(err, data) {
    res.send(data);
  });
}

exports.getMonths = function(req, res) {
  Crm.distinct("month", function(err, data) {
    res.send(data);
  });
}

exports.getDayResults = function(req, res) {
  var dateArray = new Object();
  Crm.find({"year": req.body.year, "month": req.body.month,"day": req.body.day, "owner": req.body.owner}, function(err, data) {
    dateArray.calls = data.length;
    Crm.find({"year": req.body.year, "month": req.body.month, "day": req.body.day, stage:"01 No Answer", owner: req.body.owner}, function(err, data) {
      dateArray.noAnswer = data.length;
      Crm.find({"year": req.body.year, "month": req.body.month, "day": req.body.day, stage: "02 Greeting", owner: req.body.owner}, function(err, data) {
        dateArray.greeting = data.length;
        Crm.find({"year": req.body.year, "month": req.body.month, "day": req.body.day, stage:"03 Gate Keeper", owner: req.body.owner}, function(err, data) {
          dateArray.gatekeeper = data.length;
          Crm.find({"year": req.body.year, "month": req.body.month, "day": req.body.day, stage:"04 UPS Made", owner: req.body.owner}, function(err, data) {
            dateArray.ups = data.length;
            Crm.find({"year": req.body.year, "month": req.body.month, "day": req.body.day, stage:"05 Pitch Made", owner: req.body.owner}, function(err, data) {
              dateArray.pitch = data.length;
              Crm.find({"year": req.body.year, "month": req.body.month, "day": req.body.day, stage:"06 Provided Rate", owner: req.body.owner}, function (err, data) {
                dateArray.provided = data.length;
                Crm.find({"year": req.body.year, "month": req.body.month, "day": req.body.day, "disposition": "", owner: req.body.owner}, function(err, data) {
                  dateArray.missing = data.length;
                  dateArray.salespitch = dateArray.ups + dateArray.pitch + dateArray.provided;
                  dateArray.answered = dateArray.greeting + dateArray.gatekeeper + dateArray.salespitch;
                  dateArray.owner = req.body.owner;
                  res.send(dateArray);
                });
              });
            });
          });
        });
      });
    });
  });
}

exports.getWeekResults = function(req, res) {
  var dateArray = new Object();
  Crm.find({"year": req.body.year, "week":req.body.week, "owner": req.body.owner}, function(err, data) {
    dateArray.calls = data.length;
    Crm.find({"year": req.body.year, "week":req.body.week, stage:"01 No Answer", owner: req.body.owner}, function(err, data) {
      dateArray.noAnswer = data.length;
      Crm.find({"year": req.body.year, "week":req.body.week, stage: "02 Greeting", owner: req.body.owner}, function(err, data) {
        dateArray.greeting = data.length;
        Crm.find({"year": req.body.year, "week":req.body.week, stage:"03 Gate Keeper", owner: req.body.owner}, function(err, data) {
          dateArray.gatekeeper = data.length;
          Crm.find({"year": req.body.year, "week":req.body.week, stage:"04 UPS Made", owner: req.body.owner}, function(err, data) {
            dateArray.ups = data.length;
            Crm.find({"year": req.body.year, "week":req.body.week, stage:"05 Pitch Made", owner: req.body.owner}, function(err, data) {
              dateArray.pitch = data.length;
              Crm.find({"year": req.body.year, "week":req.body.week, stage:"06 Provided Rate", owner: req.body.owner}, function (err, data) {
                dateArray.provided = data.length;
                Crm.find({"year": req.body.year, "week":req.body.week, "disposition": "", owner: req.body.owner}, function(err, data) {
                  dateArray.missing = data.length;
                  dateArray.salespitch = dateArray.ups + dateArray.pitch + dateArray.provided;
                  dateArray.answered = dateArray.greeting + dateArray.gatekeeper + dateArray.salespitch;
                  dateArray.owner = req.body.owner;
                  res.send(dateArray);
                });
              });
            });
          });
        });
      });
    });
  });
}

exports.getMonthResults = function(req, res) {
  var dateArray = new Object();
  Crm.find({"year": req.body.year, "month": req.body.month, "owner": req.body.owner}, function(err, data) {
    dateArray.calls = data.length;
    Crm.find({"year": req.body.year, "month": req.body.month, stage:"01 No Answer", owner: req.body.owner}, function(err, data) {
      dateArray.noAnswer = data.length;
      Crm.find({"year": req.body.year, "month": req.body.month, stage: "02 Greeting", owner: req.body.owner}, function(err, data) {
        dateArray.greeting = data.length;
        Crm.find({"year": req.body.year, "month": req.body.month, stage:"03 Gate Keeper", owner: req.body.owner}, function(err, data) {
          dateArray.gatekeeper = data.length;
          Crm.find({"year": req.body.year, "month": req.body.month, stage:"04 UPS Made", owner: req.body.owner}, function(err, data) {
            dateArray.ups = data.length;
            Crm.find({"year": req.body.year, "month": req.body.month, stage:"05 Pitch Made", owner: req.body.owner}, function(err, data) {
              dateArray.pitch = data.length;
              Crm.find({"year": req.body.year, "month": req.body.month, stage:"06 Provided Rate", owner: req.body.owner}, function (err, data) {
                dateArray.provided = data.length;
                Crm.find({"year": req.body.year, "month": req.body.month, "disposition": "", owner: req.body.owner}, function(err, data) {
                  dateArray.missing = data.length;
                  dateArray.salespitch = dateArray.ups + dateArray.pitch + dateArray.provided;
                  dateArray.answered = dateArray.greeting + dateArray.gatekeeper + dateArray.salespitch;
                  dateArray.owner = req.body.owner;
                  res.send(dateArray);
                });
              });
            });
          });
        });
      });
    });
  });
}
