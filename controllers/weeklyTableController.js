'use strict';
var mongoose = require('mongoose');
require('../models/crm.js');
require('../models/five9.js');
var Crm = mongoose.model('CRM');
var Occ = mongoose.model('Occupancy');
var moment = require('moment');
var gf = require('./generalFunction.js');

exports.getNumberOfDays = function(req, res) {
  console.log("Days");
  Crm.distinct("day", {year: req.body.year, month: req.body.month}, function(err, data) {
    res.send(data);
  });
}

exports.getOneDayOfMonth = function(req, res) {
  var dateArray = new Object();
  dateArray.date = req.body.day + "/" + req.body.month + "/" + req.body.year;
  Crm.find({"year": req.body.year, "month": req.body.month, "day": req.body.day}, function(err, data) {
    dateArray.calls = data.length;
    dateArray.week = data[0].week;
    dateArray.day = req.body.day;
    Crm.distinct("phone", {year: req.body.year, month: req.body.month, day: req.body.day}, function(err, data){
      dateArray.leads = data.length;
      Occ.find({"year": req.body.year, "month": req.body.month, "day": req.body.day}, function(err, data) {
        // dateArray.onCall = moment.utc((data[0].oncall + data[0].acw)*3600000).format("HH:mm:ss");
        dateArray.onCall = Math.round((data[0].oncall + data[0].acw) * 100) / 100;

        dateArray.aht = moment.utc(((data[0].oncall + data[0].acw) / dateArray.calls) * 3600000).format("HH:mm:ss");
        dateArray.occ = Math.round(((dateArray.onCall / (data[0].oncall + data[0].acw + data[0].ringing + data[0].ready)) * 100) * 100) /100;
        Crm.find({"year": req.body.year, "month": req.body.month, "day": req.body.day, stage:"01 No Answer"}, function(err, data) {
          dateArray.noAnswer = data.length;
          Crm.find({"year": req.body.year, "month": req.body.month, "day": req.body.day, stage: "02 Greeting"}, function(err, data) {
            dateArray.greeting = data.length;
            Crm.find({"year": req.body.year, "month": req.body.month, "day": req.body.day, stage:"03 Gate Keeper"}, function(err, data) {
              dateArray.gatekeeper = data.length;
              Crm.find({"year": req.body.year, "month": req.body.month, "day": req.body.day, stage:"04 UPS Made"}, function(err, data) {
                dateArray.ups = data.length;
                Crm.find({"year": req.body.year, "month": req.body.month, "day": req.body.day, stage:"05 Pitch Made"}, function(err, data) {
                  dateArray.pitch = data.length;
                  Crm.find({"year": req.body.year, "month": req.body.month, "day": req.body.day, stage:"06 Provided Rate"}, function (err, data) {
                    dateArray.provided = data.length;
                    Crm.find({"year": req.body.year, "month": req.body.month, "day": req.body.day, "disposition": ""}, function(err, data) {
                      dateArray.missing = data.length;
                      dateArray.salespitch = dateArray.ups + dateArray.pitch + dateArray.provided;
                      dateArray.answered = dateArray.greeting + dateArray.gatekeeper + dateArray.salespitch;
                      res.send(dateArray);

                    });
                  });
                });
              });
            });
          });
        });
      });
    });
  });
}
