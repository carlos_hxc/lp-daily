'use strict';

var mongoose = require('mongoose');

var crmSchema = new mongoose.Schema({
    phone: {
      type: Number,
      default: ''
    },
    disposition: {
      type: String,
      default: 'Missing Disposition'
    },
    stage: {
      type: String,
      default: '',
    },
    owner: {
      type: String,
      default: '',
    },
    status: {
      type: String,
      default: ''
    },
    batch: {
      type: String,
      default: ''
    },
    year: {
      type: Number,
      default: ''
    },
    month: {
      type: Number,
      default: ''
    },
    day: {
      type: Number,
      default: ''
    },
    week: {
      type: Number,
      default: ''
    }
});

module.exports = mongoose.model('CRM', crmSchema);
