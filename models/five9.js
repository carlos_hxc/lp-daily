'use strict';

var mongoose = require('mongoose');

var occSchema = new mongoose.Schema({
  oncall: {
    type: Number,
    default: 0
  },
  ringing: {
    type: Number,
    default: 0
  },
  acw: {
    type: Number,
    default: 0
  },
  ready: {
    type: Number,
    default: 0
  },
  day: {
    type: Number,
    default: 0
  },
  month: {
    type: Number,
    default: 0
  },
  year: {
    type: Number,
    default: 0
  },
  week: {
    type: Number,
    default: 0
  }
})

module.exports = mongoose.model('Occupancy', occSchema);
