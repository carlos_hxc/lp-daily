'use strict';
var app = angular.module("dailyreport");
app.controller("dwcCtrl", ["$scope", "$http", function($scope, $http) {
  $scope.importCRM = function() {
    var crmPath = $('#import-inputcrm').val();
    console.log(crmPath);
    var response = $http.post('/api/import/crm', {path: crmPath});
  }
  $scope.importOcc = function() {
    var occPath = $('#import-inputocc').val();
    var response = $http.post('/api/import/occ', {path: occPath});
  }
}]);
