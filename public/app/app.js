"use strict";

var app = angular.module("dailyreport", ["ngRoute"]);
console.log('AngularJS Server started');
app.config(['$routeProvider', function ($routeProvider) {
  $routeProvider
  .when("/", {
    templateUrl: "../app/DWC/dwc.html",
    controller: "weeklyCtrl"
  })
  .when("/importData", {
    templateUrl: "../app/imports/importData.html",
    controller: "importCtrl"
  }).
  when("/weekly", {
    templateUrl: "../app/weekly/weekly.html",
    controller: "weeklyCtrl"
  }).
  when('/pipeline/day', {
    templateUrl: "../app/pipeline/pipelineDaily.html",
    controller: "pipelineCtrl"
  }).
  when('/pipeline/week', {
    templateUrl:"../app/pipeline/pipelineWeekly.html",
    controller: "pipelineCtrl"
  }).
  when('/pipeline/month', {
    templateUrl: "../app/pipeline/pipelineMonth.html",
    controller: "pipelineCtrl"
  }).
  when('/batch', {
    templateUrl: "../app/batch/batch.html",
    controller: "batchCtrl"
  }).
  otherwise({
    redirectTo: "/"
  });
}]);
