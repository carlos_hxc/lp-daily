'use strict';

var app = angular.module("dailyreport");
app.controller("batchCtrl", ["$scope", "$http", function($scope, $http) {
  $scope.getData = function() {
    console.log("Init");
    $http.post('/api/batch/getFullData')
    .then(function(dataA) {
      console.log(dataA);
      $scope.batchA = dataA.data;
    }, function(err) {
      console.log(err);
    });
    $http.post('/api/batch/getBatchData', {batch: 'Batch 0'})
    .then(function(data) {
      // console.log(data);
      $scope.batch0 = data.data;
      $http.post('/api/batch/getBatchData', {batch: 'Batch 1'})
      .then(function(data1) {
        $scope.batch1 = data1.data;
      }, function(err) {
        console.log(err);
      })
    }, function(err) {
      console.log(err);
    });
  }
}]);
