'use strict';
var app = angular.module("dailyreport");
app.controller("pipelineCtrl", ["$scope", "$http", function($scope, $http) {

  $scope.$on('$viewContentLoaded', function() {
    $http.get('/api/pipeline/week/getWeeks').
    then(function(data) {
      $scope.weeks = data.data.sort();
      console.log(data.data);echo
    }, function(err) {
      console.log(err);
    });
  });

  $scope.reloadPipelineDay = function() {
    console.log("pipeline day");
    var date = $('#input-pipeline-day').val();
    var preDate = date.split("/");
    var dayD = preDate[0];
    var monthD = preDate[1];
    var yearD = preDate[2];
    var resultados = new Array();

    $http.post('/api/pipeline/day/agents', {day: preDate[0], month: preDate[1], year: preDate[2]})
    .then(function(data) {
      var owners = data.data.sort();
      for (var i = 0; i < owners.length; i++) {
        $http.post('/api/pipeline/day', {day: preDate[0], month: preDate[1], year: preDate[2], owner: owners[i]})
        .then(function(data){
          resultados.push(data.data);
          console.log(resultados);
          $scope.resultsP = resultados;
        }, function(err) {
          console.log(err);
        });
      }
    }, function(err) {
      console.log(err);
    });
  }

  $scope.reloadPipelineWeek = function() {
    console.log("pipeline week");
    var weekD = $('#select-weeks').val();
    var yearD = $('#select-year').val();
    var resultados = new Array();
    console.log(weekD);
    console.log(yearD);
    $http.post('/api/pipeline/week/agents', {week: weekD, year: yearD})
    .then(function(data) {
      var owners = data.data.sort();
      console.log(owners);
      for (var i = 0; i < owners.length; i++) {
        $http.post('/api/pipeline/week', {week: weekD, year: yearD, owner: owners[i]})
        .then(function(data){
          resultados.push(data.data);
          console.log(resultados);
          $scope.resultsW = resultados;
        }, function(err) {
          console.log(err);
        });
      }
    }, function(err) {
      console.log(err);
    });
  }

  $scope.reloadPipelineMonth = function() {
    console.log("pipeline month");
    var monthD = $('#select-month').val();
    var yearD = $('#select-year2').val();
    var resultados = new Array();
    console.log(monthD);
    console.log(yearD);
    $http.post('/api/pipeline/month/agents', {month: monthD, year: yearD})
    .then(function(data) {
      var owners = data.data.sort();
      console.log(owners);
      for (var i = 0; i < owners.length; i++) {
        $http.post('/api/pipeline/month', {month: monthD, year: yearD, owner: owners[i]})
        .then(function(data){
          resultados.push(data.data);
          console.log(resultados);
          $scope.resultsM = resultados;
        }, function(err) {
          console.log(err);
        });
      }
    }, function(err) {
      console.log(err);
    });
  }
}]);
