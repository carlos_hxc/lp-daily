'use strict';

var app = angular.module("dailyreport");
app.controller("weeklyCtrl", ["$scope", "$http", function($scope, $http){
  $scope.reloadMonthTable = function() {
    var date = $('#input-weekly').val();
    var preDate = date.split("/");
    var monthD = preDate[0];
    var yearD = preDate[1];
    var resultados = new Array();

    $http.post('/api/weeklyTable/numberOfDays', {month: monthD, year: yearD})
    .then(function(data){
      var daysOfMonth = data.data.sort();
      // console.log($scope.daysOfMonth);
      for (var i = 0; i < daysOfMonth.length; i++){
        $http.post('/api/weeklyTable', {month: monthD, year: yearD, day: daysOfMonth[i]})
        .then(function(data) {
          resultados.push(data.data);
          resultados.sort();
        }, function(err) {
          console.log(err);
        });
      }
      resultados.sort();
      $scope.results = resultados;
    }, function(err){
      console.log(err);
    });

  }

  $scope.getFullData = function() {
    var date = $('#input-home-date').val().split('/');
    $http.post('/api/DWM/day', {day: date[0], month: date[1], year: date[2]}).
    then(function(data){
      console.log(data);
      if(data.data == "404") {
        alert("The date you are trying to get, its empty. Please try with another date.");
      } else {
        $scope.daily = data.data;
        $http.post('/api/DWM/week', {year: date[2], week: data.data.week}).
        then(function(dataW) {
          $scope.weekly = dataW.data;
          $http.post('/api/DWM/month', {year: date[2], month: date[1]}).
          then(function(dataM) {
            $scope.monthly = dataM.data;
          }, function(err){
            console.log(err);
          });
        }, function(err) {
          console.log(err);
        });
      }

    }, function(err){
      console.log(err);
    });
  }
}]);
