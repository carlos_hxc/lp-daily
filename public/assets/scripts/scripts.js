$(document).ready(function() {
  $('#menu-icon').click(function() {
    $('.ui.sidebar').sidebar('toggle');
  });
  $('.input-date').datetimepicker({
    timepicker: false,
    format: 'd/m/Y'
  });
  $('.item').click(function() {
    $('.ui.sidebar').sidebar('toggle');
  })
});
