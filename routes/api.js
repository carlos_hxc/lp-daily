'use strict';

var express = require('express');
var router = express.Router();
var crmController = require('../controllers/crmController.js');
var weeklyTable = require('../controllers/weeklyTableController.js');
var occController = require('../controllers/importOccController.js');
var pipeController = require('../controllers/pipelineController.js');
var DWMController = require('../controllers/DWMController.js');
var batchController = require('../controllers/batchController.js');

//Routes
router.post('/import/crm', crmController.importData);
router.post('/import/occ', occController.importData);
router.post('/crm/getDaily', crmController.getDailyData);
router.post('/crm/getWeekly', crmController.getWeeklyData);
router.post('/crm/getMonthly', crmController.getMonthlyData);
router.post('/weeklyTable', weeklyTable.getOneDayOfMonth);
router.post('/weeklyTable/numberOfDays', weeklyTable.getNumberOfDays);
router.post('/pipeline/day/agents', pipeController.getAgents);
router.post('/pipeline/day', pipeController.getDayResults);
router.get('/pipeline/week/getWeeks', pipeController.getWeeks);
router.post('/pipeline/week', pipeController.getWeekResults);
router.post('/pipeline/week/agents', pipeController.getAgentsWeek);
router.post('/pipeline/month/agents', pipeController.getAgentsMonth);
router.get('/pipeline/month/getMonths', pipeController.getMonths);
router.post('/pipeline/month', pipeController.getMonthResults);
router.post('/DWM/day', DWMController.getOneDayOfMonth);
router.post('/DWM/week', DWMController.getOneWeekOfMonth);
router.post('/DWM/month', DWMController.getOneMonth);
router.post('/batch/getBatchData', batchController.getBatchData);
router.post('/batch/getFullData', batchController.getFullBatchData);

module.exports = router;
